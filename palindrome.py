#!/bin/python3

import math
import os
import random
import re
import sys
from collections import Counter
#
# Complete the 'countPalindromes' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#

def countPalindromes(s): 
	palindromes = Counter()
	for i in range(len(s)):
		for j in range(i+1,len(s)+1):
			if s[i:j] == s[i:j][::-1]:
				palindromes[s[i:j]] += 1
	return sum(palindromes.values())

  
if __name__ == '__main__':
	print (countPalindromes('aaa'))
