#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'minMoves' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. 2D_INTEGER_ARRAY maze
#  2. INTEGER x
#  3. INTEGER y
#

# def minMoves(maze, x, y):

paths = []
def move(maze,alice_x,alice_y,r=0,c=0,counter=0,gold=0):
	if r == alice_x and c == alice_y and gold == golds:
		#Alice
		paths.append(counter)
		return True
	elif maze[r][c] in [0,2]:
		if maze[r][c] == 2:
			#Collect gold
			gold += 1
		#mark as visited
		maze[r][c]=3
		if r<len(maze)-1:
			counter+=1
			if move(maze,alice_x,alice_y,r+1,c,counter,gold):
				return True
		if r>0:
			counter+=1
			if move(maze,alice_x,alice_y,r-1,c,counter,gold):
				return True
		if c<len(maze[r])-1:
			counter+=1
			if move(maze,alice_x,alice_y,r,c+1,counter,gold):
				return True
		if c>0:
			counter+=1
			if move(maze,alice_x,alice_y,r,c-1,counter,gold):
				return True
		#mark as block
		maze[r][c]=4    
		print("Try again")


if __name__ == '__main__':
	golds = 0
	maze = [[0,2,0],[0,0,1],[1,1,1]]
	move(maze,1,1)
	print(paths)